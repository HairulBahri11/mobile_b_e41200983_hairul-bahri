void main(List<String> args) async {
  print("Fix You - Cold Play");
  print(await "--------------------");
  print(await line1());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await "--------------------");
}

Future<String> line1() async {
  String lirik = "Lights will guide you home";
  return await Future.delayed(Duration(seconds: 5), () => (lirik));
}

Future<String> line2() async {
  String lirik = "And ignite your bones";
  return await Future.delayed(Duration(seconds: 6), () => (lirik));
}

Future<String> line3() async {
  String lirik = "I will try to fix you";
  return await Future.delayed(Duration(seconds: 7), () => (lirik));
}

Future<String> line4() async {
  String lirik = "Tears stream down your face";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> line5() async {
  String lirik = "when you lose something you cannot replace";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

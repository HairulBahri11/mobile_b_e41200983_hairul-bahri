import 'package:flutter/material.dart';

class belajarForm extends StatefulWidget {
  const belajarForm({Key? key}) : super(key: key);

  @override
  State<belajarForm> createState() => _belajarFormState();
}

class _belajarFormState extends State<belajarForm> {
  final formKey = GlobalKey<FormState>();
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Form(
        key: formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextFormField(
                  // keyboardType: TextInputType.emailAddress,
                  autofocus: true,
                  decoration: new InputDecoration(
                    hintText: "hairulb876@gmail.com",
                    labelText: "Email",
                    icon: Icon(Icons.email),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Email tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              // TextFormField(
              //   decoration: new InputDecoration(
              //     hintText: "hairulb876@gmail.com",
              //     labelText: "Email",
              //     icon: Icon(Icons.email),
              //     border: OutlineInputBorder(
              //         borderRadius: new BorderRadius.circular(5.0)),
              //   ),
              //   validator: (value) {
              //     if (value!.isEmpty) {
              //       return 'Email tidak boleh kosong';
              //     }
              //     return null;
              //   },
              // ),

              //raguuuuuuuuuuuuuuuuuuuuuuu
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextFormField(
                  obscureText: true,
                  autofocus: true,
                  decoration: new InputDecoration(
                    // hintText: "irul12345",
                    labelText: "Password",
                    icon: Icon(Icons.security),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Password tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              CheckboxListTile(
                  title: Text("Belajar Dasar Flutter"),
                  subtitle: Text("Dart, widget, http"),
                  value: nilaiCheckBox,
                  activeColor: Colors.blueGrey,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox = value!;
                    });
                  }),

              SwitchListTile(
                  title: Text("Back End Programing"),
                  subtitle: Text("PHP, Java, JavaScript, C++, Dart"),
                  value: nilaiSwitch,
                  activeTrackColor: Colors.blueAccent,
                  activeColor: Colors.blueGrey,
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  }),
              Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  }),

              RaisedButton(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Login',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                color: Colors.blue,
                onPressed: () {
                  if (formKey.currentState!.validate()) {}
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

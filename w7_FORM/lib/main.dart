import 'package:flutter/material.dart';
import 'form.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Belajar Form Flutter",
    // home: new belajarForm(),
    home: new w8(),
  ));
}

class w8 extends StatefulWidget {
  const w8({Key? key}) : super(key: key);

  @override
  State<w8> createState() => _w8State();
}

class _w8State extends State<w8> {
  List<String> agama = [
    "Islam",
    "Kristen Protestan",
    "Kristen Katolik",
    "Hindu",
    "Budha",
  ];

  String _agama = "Islam";
  String _jk = "";

  void pilih_jk(String value) {
    setState(() {
      _jk = value;
    });
  }

  void pilihAgama(String value) {
    setState(() {
      _agama = value;
    });
  }

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerPass = new TextEditingController();
  TextEditingController controllerMoto = new TextEditingController();
  void kirimData() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 7),
              child: new Text(
                "Data Diri",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  new Text("Nama Lengkap : ${controllerNama.text}"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  new Text("Password : ${controllerPass.text}"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  new Text("Moto Hidup : ${controllerMoto.text}"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  new Text("Jenis Kelamin : ${_jk}"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  new Text("Agama : ${_agama}"),
                ],
              ),
            ),
            new RaisedButton(
              child: new Text("OK", style: TextStyle(color: Colors.white)),
              onPressed: () => Navigator.pop(context),
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: new Icon(Icons.list),
        title: new Text("Data Diri"),
        backgroundColor: Colors.blue,
      ),
      body: new ListView(
        children: [
          new Container(
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              children: <Widget>[
                new TextField(
                  controller: controllerNama,
                  decoration: new InputDecoration(
                      hintText: "Isi Nama Lengkap Anda",
                      labelText: "Nama Lengkap",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0))),
                ),
                new Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new TextField(
                  controller: controllerPass,
                  decoration: new InputDecoration(
                      hintText: "Isi Password Anda",
                      labelText: "Password",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0))),
                ),
                new Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new TextField(
                  controller: controllerMoto,
                  decoration: new InputDecoration(
                      hintText: "Motto Hidup Anda",
                      labelText: "Motto Hidup",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0))),
                ),
                new Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new RadioListTile(
                  value: "laki-laki",
                  groupValue: _jk,
                  onChanged: (String? value) {
                    pilih_jk(value!);
                  },
                  activeColor: Colors.blue,
                  subtitle: new Text("Laki - Laki"),
                ),
                new RadioListTile(
                  value: "Perempuan",
                  groupValue: _jk,
                  onChanged: (String? value) {
                    pilih_jk(value!);
                  },
                  activeColor: Colors.blue,
                  subtitle: new Text("Perempuan"),
                ),
                new Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new Row(
                  children: <Widget>[
                    new Text(
                      "Agama",
                      style: TextStyle(fontSize: 18.0, color: Colors.black),
                    ),
                    new Padding(
                      padding: EdgeInsets.all(20.0),
                    ),
                    DropdownButton(
                      onChanged: (String? value) {
                        pilihAgama(value!);
                      },
                      value: _agama,
                      items: agama.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                    )
                  ],
                ),
                new Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    new RaisedButton(
                        child: new Text("OK",
                            style: TextStyle(color: Colors.white)),
                        color: Colors.blue,
                        onPressed: () {
                          kirimData();
                        }),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

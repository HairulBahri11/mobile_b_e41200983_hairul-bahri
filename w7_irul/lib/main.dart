import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new home(),
  ));
}

class home extends StatelessWidget {
  home({Key? key}) : super(key: key);
  final List<String> gambar = [
    "7F5y.gif",
    "pVkb.gif",
    "7gQj.gif",
    "T0fK.gif",
  ];

  static const Map<String, Color> colors = {
    '7F5y': Colors.greenAccent,
    '7gqj': Colors.greenAccent,
    'pVkb': Colors.greenAccent,
    'T0fK': Colors.greenAccent,
  };
  @override
  Widget build(BuildContext context) {
    timeDilation = 2.0;
    return Scaffold(
      body: new Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
              begin: FractionalOffset.bottomCenter,
              end: FractionalOffset.topCenter,
              colors: [
                Colors.yellowAccent,
                Colors.orangeAccent,
                Colors.redAccent,
              ],
            ),
          ),
          child: new PageView.builder(
            controller: new PageController(viewportFraction: 0.9),
            itemCount: gambar.length,
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                padding: EdgeInsets.all(10.0),
                child: new Material(
                    elevation: 8.0,
                    child: new Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        new Hero(
                          tag: gambar[i],
                          child: new Material(
                            child: new InkWell(
                              child: new Flexible(
                                flex: 1,
                                child: Container(
                                  color: colors.values.elementAt(i),
                                  child: Image.asset(
                                    "img/${gambar[i]}",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              onTap: () => Navigator.of(context).push(
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new halamandua(
                                            gambar: gambar[i],
                                            colors: colors.values.elementAt(i),
                                          ))),
                            ),
                          ),
                        ),
                      ],
                    )),
              );
            },
          )),
    );
  }
}

class halamandua extends StatefulWidget {
  halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  State<halamandua> createState() => _halamanduaState();
}

class _halamanduaState extends State<halamandua> {
  Color warna = Colors.grey;
  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("IRUL"),
        backgroundColor: Colors.deepPurpleAccent,
        actions: <Widget>[
          new PopupMenuButton<Pilihan>(
            onSelected: _pilihannya,
            itemBuilder: (BuildContext context) {
              return listPilihan.map((Pilihan x) {
                return PopupMenuItem<Pilihan>(
                  child: new Text(x.teks),
                  value: x,
                );
              }).toList();
            },
          )
        ],
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                gradient: new RadialGradient(center: Alignment.center, colors: [
              Colors.purple,
              Colors.white,
              Colors.purpleAccent
            ])),
          ),
          new Center(
            child: new Hero(
                tag: widget.gambar,
                child: new ClipOval(
                  child: new SizedBox(
                    width: 200.0,
                    height: 200.0,
                    child: new Material(
                      child: new InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: new Flexible(
                            flex: 1,
                            child: Container(
                              color: widget.colors,
                              child: new Image.asset(
                                "img/${widget.gambar}",
                                fit: BoxFit.cover,
                              ),
                            )),
                      ),
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Green", warna: Colors.green),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];

// class _halamanduaState extends State<halamandua> {
//   Color warna = Colors.grey;

//   void _pilihannya(Pilihan pilihan) {
//     setState(() {
//       warna = pilihan.warna;
//     });
//   }
// }

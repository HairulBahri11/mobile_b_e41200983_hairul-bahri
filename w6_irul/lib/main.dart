import 'package:flutter/material.dart';
import 'package:w6_irul/pages/navbar_colrow.dart';
import 'package:w6_irul/pages/navbar_colrow.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: navbar_colrow(),
    );
  }
}

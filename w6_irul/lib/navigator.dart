import 'package:flutter/material.dart';
import 'package:w6_irul/pages/about.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: '/',
    routes: <String, WidgetBuilder>{
      '/': (context) => navigator(),
      '/about': (context) => about(),
      '/halaman404': (context) => ErrorPage(),
    },
  ));
}

class navigator extends StatelessWidget {
  const navigator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Belajar Routing'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                // Route route = MaterialPageRoute(builder: (context) => about());
                Navigator.pushNamed(context, '/about');
              },
              child: Text(
                "About Page",
              ),
            ),
            ElevatedButton(
              onPressed: () {
                // Route route = MaterialPageRoute(builder: (context) => about());
                Navigator.pushNamed(context, '/halaman404');
              },
              child: Text("Other Pages"),
            ),
          ],
        ),
      ),
    );
  }
}

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Error Page'),
      ),
      body: Center(child: Text('Error Page')),
    );
  }
}

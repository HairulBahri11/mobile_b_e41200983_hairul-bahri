import 'package:flutter/material.dart';

class navbar_colrow extends StatelessWidget {
  const navbar_colrow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.dashboard),
          title: Text('Belajar Material App Scaffold'),
          actions: <Widget>[
            Icon(Icons.search),
          ],
          actionsIconTheme: IconThemeData(color: Colors.amber),
          backgroundColor: Colors.redAccent,
          bottom: PreferredSize(
            child: Container(
              color: Colors.orange,
              height: 4.0,
            ),
            preferredSize: Size.fromHeight(4.0),
          ),
          centerTitle: true,
        ),
        //floating action button
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          child: Text('+'),
          onPressed: () {},
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 60,
              height: 60,
              decoration:
                  BoxDecoration(color: Colors.orange, shape: BoxShape.circle),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.redAccent, shape: BoxShape.circle),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.orange, shape: BoxShape.circle),
                ),
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.redAccent, shape: BoxShape.circle),
                ),
                Container(
                  width: 60,
                  height: 60,
                  decoration:
                      BoxDecoration(color: Colors.red, shape: BoxShape.circle),
                )
              ],
            )
          ],
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

import 'dart:io';

//Hairul Bahri
//Menerapkan Perulangan dan Percabangan

void main() {
  print("");
  print("");
  print("Masukkan Nama : ");

//menerapkan inputan
  String inputText = stdin.readLineSync()!;
  print("Nama ku adalah " + inputText.toString());
  print("");

  tipeData();
  print("");
  percabngan();
  print("");
  perulangan();
  print("");
  operasiMat();
  print("");
}

//menampilkan data dari beberapa tipe data
void tipeData() {
  var prodi = "Teknik Informatika"; //String
  var angka = 18; //number
  var hariSabtu = false; // Boolean
  print(prodi);
  print(angka);
  print(hariSabtu);
}

//operasi matematika
void operasiMat() {
  var operand1 = 5;
  var operand2 = 10;
  print("Operasi a = 5 dan b = 10");
  print(operand1 + operand2);
  print(operand1 - operand2);
  print(operand1 * operand2);
  print(operand1 / operand2);
}

//Percabangan
void percabngan() {
//If and Else
  var today = "minggu";
  if (today == "sabtu") {
    print("Today is sabtu");
  } else {
    print("Today is : " + today);
  }

  //if, else if dan else
  var menu = "Bakso dan Tahu";

  if (menu == "Bakso") {
    print("Your menu is Bakso");
  } else if (menu == "Bakso dan Tahu") {
    print("Your menu is Bakso dan Tahu");
  } else {
    print("Your menu is Unknown");
  }

  //if, else if dan else
  var nilai = 92;
  if (nilai == 100) {
    print("Your Grade is A+");
  } else if (nilai >= 90) {
    print("Your Grade is A-");
  } else if (nilai >= 70 && nilai < 90) {
    print("Your Grade Is B+");
  } else {
    print("your Grade Is C");
  }

  //Switch Case
  var buttonPushed = 1;
  switch (buttonPushed) {
    case 1:
      {
        print('matikan TV!');
        break;
      }
    case 2:
      {
        print('turunkan volume TV!');
        break;
      }
    case 3:
      {
        print('tingkatkan volume TV!');
        break;
      }
    case 4:
      {
        print('matikan suara TV!');
        break;
      }
    default:
      {
        print('Tidak terjadi apa-apa');
      }
  }
}

void perulangan() {
  //for
  var jumlah = 0;
  for (var deret = 5; deret > 0; deret--) {
    jumlah += deret;
    print('Jumlah saat ini: ' + jumlah.toString());
  }
  print('Jumlah terakhir: ' + jumlah.toString());

//for in
  var matkul = ["Statistika", "Struktur Data", "English", "Pemrograman Dasar"];
  for (var data in matkul) {
    print(data);
  }

  //while do
  var deret = 5;
  var qty = 0;
  while (deret > 0) {
    // Loop akan terus berjalan selama nilai deret masih di atas 0
    qty += deret; // Menambahkan nilai variable jumlah dengan angka deret
    deret--; // Mengubah nilai deret dengan mengurangi 1
    print('Jumlah saat ini: ' + qty.toString());
  }
}

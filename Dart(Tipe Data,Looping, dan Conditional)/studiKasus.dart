import 'dart:io';

void main() {
  print("");
  kasus1();
  print("");
  kasus2();
  print("");
  kasus3();
}

void kasus1() {
  //for loop
  for (int i = 1; i < 21; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
      print("I Love Coding");
    } else if (i % 2 == 0) {
      print("Berkualitas");
    } else if (i % 2 == 1) {
      print("Santai");
    }
  }
}

void kasus2() {
//Studi kasus percabangan
  print("Apakah anda akan menginstall Dart? Y/T");
  // Reading name
  var install = stdin.readLineSync();

  install == "Y"
      ? print("Terinstall")
      : install == "T"
          ? print("Not Yet")
          : print("Gagal");
}

void kasus3() {
//case : WereWolf
  print("Masukkan Nama Anda?");
  // Reading name
  var name = stdin.readLineSync();

  if (name == "") {
    print("Nama harus diisi");
  } else {
    print(
        "Pilih Peran Anda, ketikkan angka? \n1. Penyihir \n2. Guard \n3. Werewolf");
    var peran = stdin.readLineSync();

    if (peran == "") {
      print("Hallo, $name Pilih peranmu untuk memulai game!");
    } else if (peran == "1") {
      print(
          "Selamat datang di Dunia Werewolf, $name! \nHalo Penyihir $name, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "2") {
      print(
          "Selamat datang di Dunia Werewolf, $name! \nHalo Guard $name, kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if (peran == "3") {
      print("Selamat datang di Dunia Werewolf, $name"
          "\nHalo Werewolf $name, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Tidak ada yang dipilih");
    }
  }
}
